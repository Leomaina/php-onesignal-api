<?php

namespace le0m\OneSignal\models;

use le0m\webapi\Model;


/**
 * Class Notification
 *
 * @property string $app_id OneSignal application ID
 * @property array $include_player_ids target player IDs
 * @property array $contents push message content
 */
class Notification extends Model
{
	public function attributes()
	{
		return [
			'app_id' => [
				'required',
				'types' => ['string']
			],
			'include_player_ids' => [
				'required',
				'types' => ['array']
			],
			'contents' => [
				'required',
				'types' => ['array']
			]
		];
	}
}
