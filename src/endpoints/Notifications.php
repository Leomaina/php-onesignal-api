<?php

namespace le0m\OneSignal\endpoints;

use GuzzleHttp\Message\ResponseInterface;

use le0m\webapi\BaseEndpoint;
use le0m\OneSignal\models\Notification;


class Notifications extends BaseEndpoint
{
	/**
	 * Send a notification push.
	 *
	 * @param Notification $push notification object to send
	 * @param bool $validate whether to validate model before sending request
	 *
	 * @return ResponseInterface|null null on validation errors (use {@link Model::getErrors()} to retrieve error messages)
	 */
	public function sendPush($push, $validate = true)
	{
		if ($validate && !$push->validate())
			return null;

		return $this->getClient()->request('post', 'notifications', [], $push);
	}
}
