<?php

namespace le0m\OneSignal;

use le0m\webapi\BaseClient;
use le0m\OneSignal\endpoints\Notifications;


/**
 * Class OneSignal
 */
class OneSignal extends BaseClient
{
	/* @var string $_rest_api_key authorization key */
	private $_rest_api_key;


	/**
	 * OneSignal constructor.
	 * Set REST API key.
	 *
	 * @param string $uri OneSignal base API URL
	 * @param string $key OneSignal REST API key
	 */
	public function __construct($uri, $key)
	{
		parent::__construct($uri);

		$this->_rest_api_key = $key;
	}

	/**
	 * @return array custom headers
	 */
	public function headers()
	{
		return array_merge(parent::headers(), [
			'Authorization' => 'Base ' . $this->_rest_api_key
		]);
	}

	/**
	 * Get Notifications endpoint.
	 *
	 * @return Notifications the endpoint object
	 */
	public function notifications()
	{
		return new Notifications($this);
	}
}
